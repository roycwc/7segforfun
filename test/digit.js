const assert = require('assert')
const Digit = require('../digit')

describe('digit', ()=>{
  it('should recognize a 7 segment input 0',()=>{
    let myDigit = new Digit('1110111')
    assert(myDigit.toNum() == 0)
  })
  it('should recognize a 7 segment input _1',()=>{
    let myDigit = new Digit('0010010')
    assert(myDigit.toNum() == 1)
  })

  it('should recognize a 7 segment input 1_',()=>{
    let myDigit = new Digit('0100100')
    assert(myDigit.toNum() == 1)
  })

  it('should recognize a 7 segment input 2',()=>{
    let myDigit = new Digit('1011101')
    assert(myDigit.toNum() == 2)
  })

  it('should recognize a 7 segment input 3',()=>{
    let myDigit = new Digit('1011011')
    assert(myDigit.toNum() == 3)
  })

  it('should recognize a 7 segment input 4',()=>{
    let myDigit = new Digit('0111010')
    assert(myDigit.toNum() == 4)
  })

  it('should recognize a 7 segment input 5',()=>{
    let myDigit = new Digit('1101011')
    assert(myDigit.toNum() == 5)
  })

  it('should recognize a 7 segment input 6',()=>{
    let myDigit = new Digit('1101111')
    assert(myDigit.toNum() == 6)
  })

  it('should recognize a 7 segment input 6',()=>{
    let myDigit = new Digit('0101111')
    assert(myDigit.toNum() == 6)
  })

  it('should recognize a 7 segment input 7',()=>{
    let myDigit = new Digit('1010010')
    assert(myDigit.toNum() == 7)
  })

  it('should recognize a 7 segment input _7',()=>{
    let myDigit = new Digit('1110010')
    assert(myDigit.toNum() == 7)
  })

  it('should recognize a 7 segment input 8',()=>{
    let myDigit = new Digit('1111111')
    assert(myDigit.toNum() == 8)
  })

  it('should recognize a 7 segment input 9',()=>{
    let myDigit = new Digit('1111011')
    assert(myDigit.toNum() == 9)
  })

  it('should recognize a 7 segment input _9',()=>{
    let myDigit = new Digit('1111010')
    assert(myDigit.toNum() == 9)
  })
  
  it('should diff 0 and 9 by +1 -1 segment difference',()=>{
    let digit1 = new Digit(0)
    let digit2 = new Digit('1111011')
    let diff = digit1.segmentDiff(digit2) 
    assert(diff.more==1 && diff.less==1)

  })
  
})
