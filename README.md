# 7-segment movement solver
Solving 3 digit 7-segment by movement segments only twice



[![Build Status](https://travis-ci.org/roycwc/7segforfun.svg?branch=master)](https://travis-ci.org/roycwc/7segforfun)

![7-segment problem](https://ir0.mobify.com/540/https://drop.wtako.net/file/02b411604d697ca0e2038202616b885aea187754.jpg)

# How to test
```
npm test
```
