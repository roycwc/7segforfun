class Digit{
  
  constructor(input){
     
    this.SEGS = [
      {s:'1110111',n:0},
      {s:'0010010',n:1},
      {s:'0100100',n:1},
      {s:'1011101',n:2},
      {s:'1011011',n:3},
      {s:'0111010',n:4},
      {s:'1101011',n:5},
      {s:'1101111',n:6},
      {s:'0101111',n:6},
      {s:'1010010',n:7},
      {s:'1110010',n:7},
      {s:'1111111',n:8},
      {s:'1111010',n:9},
      {s:'1111011',n:9}
    ]

    if (input.length==7){
      this.fromSeg(input)
      return
    }

    this.fromNum(input)
  } 

  fromSeg(segments){
    let foundNum = this.SEGS.filter(it=>it.s==segments)
    if (foundNum.length != 1) throw 'Not a valid segment'
    this.num = foundNum[0].n
    this.seg = segments
  }
  fromNum(number){
    if (!Number.isInteger(number) || number < 0 || number > 9) throw 'Not a digit'
    this.seg = this.SEGS.filter(it=>it.n==number)[0].s
    this.num = number
  }
  toNum(){
    return this.num
  }

  segmentDiff(n){
    let diff = {less:0,more:0}
    let range = [0,1,2,3,4,5,6]
    range.map((i)=>{
      let segDiff = parseInt(this.seg[i]) - parseInt(n.seg[i])
      if (segDiff>0) diff.less++
      if (segDiff<0) diff.more++
    })
    return diff
  }

  printSeg(){
    console.log(this.seg[0] == '1' ? ' -- ' : '    ')
    console.log(this.seg[1] == '1' ? '| ' : '   ' , this.seg[2] == '1' ? '|' : ' ')
    console.log(this.seg[3] == '1' ? ' -- ' : '    ')
    console.log(this.seg[4] == '1' ? '| ' : '   ' , this.seg[5] == '1' ? '|' : ' ')
    console.log(this.seg[6] == '1'? ' -- ' : '    ')
  }
}

module.exports = Digit
